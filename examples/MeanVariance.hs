{-# LANGUAGE FlexibleContexts #-}

module MeanVariance where

import qualified Data.Vector as V
import Data.Vector.Generic as G
import qualified Data.Vector.Unboxed as U
import Numeric.AD.Double
import Numeric.GSL.Root
import Packera

{-
-}
psi :: (Mode a, Vector v a, Floating a) => Scalar a -> v a -> v a
psi x v =
    let d = auto x - v ! 0 -- mu
     in fromList
            [ d
            , d ^ (2 :: Integer) - v ! 1
            , log (v ! 1) - v ! 2
            ]
{-# INLINE psi #-}

ee :: (Foldable f) => f Double -> Result Double
ee =
    mestimate
        ( FindRoots
            ( \f ->
                U.fromList
                    ( fst
                        ( root
                            DNewton
                            0.0001
                            20
                            (U.toList . f . U.fromList)
                            [50.0, 25.0, 30]
                        )
                    )
            )
        )
        ( Jacobian $ \xs ->
            jacobian
                ( convert
                    . psiSumF psi (V.map auto (V.fromList [0.0, 0.0, 0.0])) xs
                )
                . convert
        )
        psi
{-# INLINEABLE ee #-}

testVals :: V.Vector Double
testVals = V.fromList [1 .. 100]

-- V.replicate 100 (1.0 :: Double)
