{-# LANGUAGE FlexibleContexts #-}

module GLM where

import qualified Data.Vector as V
import Data.Vector.Generic as G
import qualified Data.Vector.Unboxed as U
import Numeric.AD.Double
import Numeric.GSL.Root
import Packera

{-
-}
psi
    :: (Vector v (Scalar c), Mode c, Vector v c)
    => (c -> c)
    -> (Scalar c, v (Scalar c))
    -> v c
    -> v c
psi invlink (y, x) θ =
    let xs = G.map auto x
        lp = G.foldl' (+) 0 (G.zipWith (*) xs θ)
     in G.map ((auto y - invlink lp) *) xs
{-# INLINE psi #-}

logit :: (Num c, Floating c) => c -> c
logit x = log (x / (1 - x))

logistic :: (Num c, Floating c) => c -> c
logistic x = 1 / (1 + exp (negate x))
{-# INLINE logistic #-}

ee :: (Foldable f) => f (Double, U.Vector Double) -> Result Double
ee =
    mestimate
        ( FindRoots
            ( \f ->
                U.fromList
                    ( fst
                        ( root
                            DNewton
                            0.0001
                            20
                            (U.toList . f . U.fromList)
                            [1, 1.0]
                        )
                    )
            )
        )
        ( Jacobian $ \xs ->
            jacobian
                ( convert
                    . psiSumF
                        (\(y, x) -> psi logistic (auto y, convert x))
                        (V.map auto (V.fromList [0.0, 0.0]))
                        xs
                )
                . convert
        )
        (psi logistic)
{-# INLINEABLE ee #-}

testVals :: [(Double, U.Vector Double)]
testVals =
    [ (1, U.fromList [1, 1.0])
    , (1, U.fromList [1, 1.0])
    , (1, U.fromList [1, 1.0])
    , (1, U.fromList [1, 1.0])
    , (1, U.fromList [1, 0.0])
    , (1, U.fromList [1, 0.0])
    , (1, U.fromList [1, 1.0])
    , (1, U.fromList [1, 0.0])
    , (1, U.fromList [1, 0.0])
    , (0, U.fromList [1, 0.0])
    , (0, U.fromList [1, 1.0])
    , (0, U.fromList [1, 0.0])
    , (0, U.fromList [1, 0.0])
    , (0, U.fromList [1, 0.0])
    , (0, U.fromList [1, 1.0])
    , (0, U.fromList [1, 0.0])
    , (0, U.fromList [1, 1.0])
    , (0, U.fromList [1, 0.0])
    , (0, U.fromList [1, 1.0])
    , (0, U.fromList [1, 0.0])
    ]
