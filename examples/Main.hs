{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import GLM

import qualified Data.ByteString.Lazy as BL
import Data.Csv
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U

-- main :: IO ()
-- main = print (ee testVals)

main :: IO ()
main = do
    csvData <- BL.readFile "examples/data/glm-logistic_100000.csv"
    case decode HasHeader csvData of
        Left err -> putStrLn err
        Right v -> do
            print . ee
                =<< V.forM
                    v
                    (\(y :: Double, x1 :: Double, x2 :: Double) -> pure (y, U.fromList [x1, x2]))

-- V.forM_ v $ \ (y :: Double, x1 :: Double, x2 :: Double) ->
-- ee
-- putStrLn $ show y ++ " earns " ++ show x1 ++ " dollars"
