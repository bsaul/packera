{-# LANGUAGE TemplateHaskell #-}

-- {-# LANGUAGE NoImplicitPrelude #-}

module Packera.TestInternal where

import qualified Data.Vector as V
import Packera.Internal
import Test.QuickCheck
import Test.Tasty
import Test.Tasty.QuickCheck

-- Outer product of vectors (of same length)
-- is equal to kronecker product
prop_op_kronecker :: [Double] -> [Double] -> Property
prop_op_kronecker xs ys =
    not (null xs) && length ys >= length xs ==>
        let x = V.fromList xs
            y = V.fromList $ take (length xs) ys
         in V.concat (V.toList (x ⊗ y)) === V.concatMap (\z -> V.map (* z) y) x

-- Element wise vector addition is commutative
prop_vaddition_comm :: [Double] -> [Double] -> Property
prop_vaddition_comm xs ys =
    not (null xs) && length ys >= length xs ==>
        let x = V.fromList xs
            y = V.fromList $ take (length xs) ys
         in x .+ y === y .+ x

-- See
-- https://hackage.haskell.org/package/QuickCheck-2.14.2/docs/Test-QuickCheck.html#v:quickCheckAll
-- for the reason of the weird `return []` here
return []
internalTests :: TestTree
internalTests = testProperties "Packera Internals" $allProperties
