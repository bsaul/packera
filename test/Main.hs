module Main (main) where

import Packera.TestInternal
import Test.Tasty (defaultMain)

main :: IO ()
main = defaultMain internalTests
