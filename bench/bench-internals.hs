{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE FlexibleContexts #-}

module Main where

import Criterion.Main

import qualified Data.Vector as V
import Data.Vector.Generic as G

import qualified Data.Vector.Unboxed as U
import Packera.Internal

setupEnv :: IO (V.Vector Double)
setupEnv = do
    pure $ V.replicate (10 ^ (6 :: Int)) (1.0 :: Double)

foo :: (Vector v Double) => Double -> v Double -> v Double
foo x v =
    let !d = x - v ! 0 -- mu
    -- in singleton d G.++ singleton ( d^(2 :: Integer) - v ! 1)
     in fromList
            [ d
            , d ^ (2 :: Integer) - v ! 1
            ]
{-# INLINE foo #-}

zeroU :: U.Vector Double
zeroU = fromList [0.0, 0.0]

zeroV :: V.Vector Double
zeroV = fromList [0.0, 0.0]

-- Our benchmark harness.
main :: IO ()
main =
    defaultMain
        [ env setupEnv $ \x ->
            bgroup
                "psiSum"
                [ bench "Unboxed Vector" $ nf (psiSum foo zeroU) x
                , bench "Boxed Vector" $ nf (psiSum foo zeroV) x
                ]
        , env setupEnv $ \x ->
            bgroup
                "psiSumF"
                [ bench "Unboxed Vector" $ nf (psiSumF foo zeroU x) zeroU
                , bench "Boxed Vector" $ nf (psiSumF foo zeroV x) zeroV
                ]
        , env setupEnv $ \x ->
            bgroup
                "bread 'Matrix'"
                [ bench "makeBreadk Vector" $ nf (makeBreadk foo zeroV) x
                , bench "makeBreadk Unboxed" $ nf (makeBreadk foo zeroU) x
                , bench "makeBread Vector" $ nf (makeBread foo zeroV :: V.Vector Double -> [V.Vector Double]) x
                , bench "makeBread Unboxed" $ nf (makeBread foo zeroU :: V.Vector Double -> [U.Vector Double]) x
                ]
        ]
