{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Criterion.Main
import qualified Data.Vector as V
import Data.Vector.Generic as G
import qualified Data.Vector.Storable as S
import qualified Data.Vector.Unboxed as U
import Numeric.GSL.Root
import Packera.Internal

setupEnv :: IO (V.Vector Double)
setupEnv = do
    pure $ V.replicate (10 ^ (6 :: Int)) (1.0 :: Double)

foo :: (Vector v Double) => Double -> v Double -> v Double
foo x v =
    let !d = x - v ! 0 -- mu
     in fromList
            [ d
            , d ^ (2 :: Integer) - v ! 1
            ]
{-# INLINE foo #-}

zeroU :: U.Vector Double
zeroU = fromList [0.0, 0.0]

zeroV :: V.Vector Double
zeroV = fromList [0.0, 0.0]

gsl :: forall v. (Vector v Double) => (v Double -> v Double) -> v Double
gsl f = G.fromList $ fst (root DNewton 0.0001 20 (G.toList . f . G.fromList) [0.0, 0.0])

main :: IO ()
main =
    defaultMain
        [ env setupEnv $ \x ->
            bgroup
                "gsl"
                [ bench "Vector" $ nf (\v -> gsl @V.Vector (\θ -> psiSum foo θ v)) x
                , bench "Unboxed Vector" $ nf (\v -> gsl @U.Vector (\θ -> psiSum foo θ v)) x
                , bench "Storable Vector" $ nf (\v -> gsl @S.Vector (\θ -> psiSum foo θ v)) x
                ]
        ]
