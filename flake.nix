{
  description = "Packera: m-estimation in Haskell";
  nixConfig = { bash-prompt = "λ "; };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let

        packageName = "packera";

        pkgs = nixpkgs.legacyPackages.${system};

        # include delicatessen for comparisons/bencharking
        deli = ps:
          with ps;
          [
            # 
            (pkgs.python3Packages.buildPythonPackage rec {
              pname = "delicatessen";
              version = "v1.2";
              src = pkgs.fetchFromGitHub {
                owner = "pzivich";
                repo = "${pname}";
                rev = "34772d6523be7c5363f13d1e9c5d19eb2ef2f6bf";
                sha256 = "sha256-oFZanmkDT7iJIDQ6r8Zui3rR80oB9ZzXxZhrRsYyLaE=";
              };
              doCheck = false;
              propagatedBuildInputs = [
                # Specify dependencies
                pkgs.python3Packages.numpy
                pkgs.python3Packages.scipy
              ];
            })
          ];

      in {

        packages.${packageName} =
          pkgs.haskellPackages.callCabal2nix packageName self rec {
            # Dependency overrides go here
          };

        defaultPackage = self.packages.${system}.${packageName};

        devShells.default = pkgs.mkShell {
          buildInputs = [

            # Haskell
            pkgs.haskellPackages.haskell-language-server
            pkgs.haskellPackages.ghcid
            pkgs.haskellPackages.cabal-install
            pkgs.llvm

            # System dependencies for hmatrix/hmatrix-gsl
            pkgs.blas
            pkgs.lapack
            pkgs.gsl

            # Testing/benchmarking
            pkgs.hyperfine

            # R and packages
            pkgs.R
            pkgs.rPackages.broom
            pkgs.rPackages.devtools
            pkgs.rPackages.dplyr
            pkgs.rPackages.geepack
            pkgs.rPackages.geex
            pkgs.rPackages.ggplot2
            pkgs.rPackages.ggbeeswarm
            pkgs.rPackages.igraph
            pkgs.rPackages.jsonlite
            pkgs.rPackages.languageserver
            pkgs.rPackages.microbenchmark
            pkgs.rPackages.purrr

            # python
            (pkgs.python3.withPackages deli)

            # developer tools
            pkgs.treefmt
            pkgs.nixfmt
          ];
          inputsFrom = builtins.attrValues self.packages.${system};
        };
      });
}
