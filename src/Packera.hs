{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoImplicitPrelude #-}

{- |
Module      : Packera
Description : M-estimation/Estimating Equations
License     : MIT
Maintainer  : bradleysaul@fastmail.com
-}
module Packera where

import Control.Foldl (Fold (..), Foldable, fold)
import Data.Eq (Eq)
import Data.Function (const, ($), (.))
import Data.Functor (Functor (fmap))
import Data.Maybe (Maybe (..))
import qualified Data.Vector as V
import Data.Vector.Generic (Vector, length, replicate)
import qualified Data.Vector.Unboxed as U
import GHC.Generics (Generic)
import GHC.Num (Num (negate))
import GHC.Show (Show)
import Numeric.LinearAlgebra as LA hiding (Vector, step)
import Packera.Internal

{- |
M-estimation
-}
mestimate
    :: (Foldable f, Num a, Vector U.Vector a, U.Unbox a)
    => -- \^ root finder
    RootControl U.Vector a
    -- ^ jacobian function
    -> JacobianControl f d U.Vector V.Vector a
    -- ^ ψ function
    -> (d -> U.Vector a -> U.Vector a)
    -- ^ data
    -> f d
    -> Result a
mestimate root jac ψ xs =
    let b θ = makeBread ψ θ xs
        !θ' = case root of
            FindRoots f -> f (\θ -> psiSum ψ θ xs)
            GivenRoots r -> r
        !sndwch = case jac of
            NoCovariance -> Nothing
            Jacobian δ ->
                Just $
                    MkSandwich
                        { bread = V.toList $ V.map (V.toList . V.map negate) (δ xs θ')
                        , veggies = fmap U.toList (b θ')
                        }
     in MkResult
            { roots = U.toList θ'
            , covariance = sndwch
            }
{-# INLINE mestimate #-}

-- | Controls the root finding algorithm for 'mestimate'.
data RootControl v a
    = -- | Tells 'mestimate' to find the roots of the estimating equation
      --   using the provided function.
      FindRoots ((v a -> v a) -> v a)
    | -- | Tells 'mestimate' to computing the estimating equation
      --   using the given vector of values.
      GivenRoots !(v a)

{- | Controls how derivatives (necessary for covariance estimation)
are computed by the 'mestimate' function.
The type parameters are:

[@f@] : The type of container for the data
[@d@] : type of data
[@w@] : type of vector for input parameters
[@v@] : type of vector for output parameters
[@a@] : type of parameter
-}
data JacobianControl f d w v a
    = -- | Do not compute the covariance of the estimating equation parameters.
      NoCovariance
    | -- | Use the given function to compute the covariance
      Jacobian (f d -> w a -> v (v a))

{- | Container for the parts of the sandwich covariance matrix.
  Use 'sandwich' to convert to a matrix.
-}
data Sandwich a = MkSandwich
    { bread :: ![[a]]
    , veggies :: ![[a]]
    }
    deriving (Eq, Show, Generic)

-- | Type for the result of 'mestimate'
data Result a = MkResult
    { roots :: ![a]
    , covariance :: Maybe (Sandwich a)
    }
    deriving (Eq, Show, Generic)

-- | Compute the sandwich covariance matrix
sandwich :: (Numeric a, Field a) => Sandwich a -> LA.Matrix a
sandwich x =
    let !a = inv $ fromLists (bread x)
        !b = fromLists (veggies x)
     in a <> b <> tr a

{- |
Create a Fold that sums over the evaluated psi function
given a vector of parameters
-}
psiFold
    :: (Vector v a, Num a)
    => (d -> v a -> v a)
    -> v a
    -> Fold d (v a)
psiFold psi theta = Fold step begin done
  where
    begin = replicate (length theta) 0
    step x a = x .+ psi a theta
    done x = x
{-# INLINE psiFold #-}

-- | Create 'psiFold' and compute it
psiSum
    :: (Vector v a, Num a, Foldable f)
    => (d -> v a -> v a)
    -> v a
    -> f d
    -> v a
psiSum psi theta = fold (psiFold psi theta)
{-# INLINE psiSum #-}

{- |
Create a Fold that sums over the psi function
given a vector of parameters
-}
psiFoldF
    :: (Vector v a, Num a)
    => (d -> v a -> v a)
    -> v a
    -> Fold d (v a -> v a)
psiFoldF psi theta = Fold step begin done
  where
    begin = const (replicate (length theta) 0)
    step f a v = f v .+ psi a v
    done x = x
{-# INLINE psiFoldF #-}

-- | Create 'psiFoldF' and compute it
psiSumF
    :: (Vector v a, Num a, Foldable f)
    => (d -> v a -> v a)
    -> v a
    -> f d
    -> (v a -> v a)
psiSumF psi theta = fold (psiFoldF psi theta)
{-# INLINE psiSumF #-}
