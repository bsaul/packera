{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}

{- |
Module      : Packera.Internal
Description : Internal Functions used in packera
License     : MIT
Maintainer  : bradleysaul@fastmail.com
-}
module Packera.Internal where

import Control.Foldl (Fold (..), Foldable, fold)
import qualified Data.List as L
import Data.Vector.Generic
import GHC.Int
import GHC.Num (Num ((*), (+)))

-- | The outer product of two vectors
(⊗)
    :: (Vector v a, Vector w a, Vector w (v a), Num a)
    => w a
    -> v a
    -> w (v a)
(⊗) xs ys = map (\x -> map (* x) ys) xs
{-# INLINE (⊗) #-}

-- | The Kronecker product of two vectors
(<*>) :: (Vector v a, Num a) => v a -> v a -> v a
(<*>) xs ys = concatMap (\x -> map (* x) ys) xs
{-# INLINE (<*>) #-}

-- | Elementwise vector addition
(.+) :: (Vector v a, Num a) => v a -> v a -> v a
(.+) = zipWith (+)
{-# INLINE (.+) #-}

-- | Bread computation via Kronecker product is MUCH faster
breadFold
    :: (Vector v a, Num a)
    => (d -> v a -> v a)
    -> v a
    -> Fold d (v a)
breadFold psi theta = Fold step begin done
  where
    begin = replicate (length theta * length theta) 0
    step x a = let !z = psi a theta in (z <*> z) .+ x
    done x = x
{-# INLINE breadFold #-}

makeBread
    :: (Vector v a, Num a, Foldable f)
    => (d -> v a -> v a)
    -> v a
    -> f d
    -> [v a]
makeBread psi theta xs =
    knead (length theta) (fold (breadFold psi theta) xs)
{-# INLINE makeBread #-}

parts :: Int -> [a] -> [[a]]
parts _ [] = []
parts p xs = L.take p xs : parts p (L.drop p xs)
{-# INLINE parts #-}

knead
    :: (Vector v a, Num a)
    => Int
    -> v a
    -> [v a]
knead p vals = L.map fromList (parts p (toList vals))
{-# INLINE knead #-}
