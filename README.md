# Packera

`packera` is a
[Haskell](https://www.haskell.org/)
library for writing estimating equations
which can used to obtain point and variance estimates
for parameters in the equations.

The `packera` library does not, by itself,
provide functions for evaluating
point estimates by root finding
nor variance estimates by finding derivatives.
Instead, the user must provide such functionality,
either by importing functions from another library
or writing their own.
The `examples` directory includes examples
of root finding using the
[`hmatrix-gsl`](https://hackage.haskell.org/package/hmatrix-gsl)
package
and evaluating derivatives using the
[`ad`](https://hackage.haskell.org/package/ad)
package.

## What is M-estimation?

M-estimation is a widely applicable statistical tool
that encompasses ideas such as:

* the empirical “sandwich” variance estimator
* generalized estimating equations (GEE)
* many maximum likelihood estimators
* robust regression

See
[Stefanski and Boos (2002)](https://www.jstor.org/stable/3087324)
for an excellent introduction.

## Usage

See the `examples` directory for examples.
The source code for the
[lobelia](https://gitlab.com/bsaul/lobelia)
project contains an example of a more complicated
estimating equation.

## Related work

* [`geex`](https://bsaul.github.io/geex/)
R package
* [`delicatessen`](https://github.com/pzivich/Delicatessen)
python package
* [`RobustModels.jl`](https://github.com/getzze/RobustModels.jl)
Julia package
* [`MEstimation`](https://github.com/ikosmidis/MEstimation.jl)
Julia package

## Get help

Please feel free to
[contact me](mailto:fun.car9925@fastmail.com)
by email or
[submit an issue](https://gitlab.com/bsaul/packera/-/issues)
with questions/bugs/etc.
